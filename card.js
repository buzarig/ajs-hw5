const loader = document.querySelector(".load");
const posts = document.querySelector(".posts");

export class Card {
  constructor(title, name, email, body, postId) {
    this.name = name;
    this.email = email;
    this.title = title;
    this.body = body;
    this.postId = postId;

    this.post = document.createElement("div");
    this.remover = document.createElement("span");
    this.editor = document.createElement("span");
    this.editorOk = document.createElement("span");
    this.titleElement = document.createElement("h1");
    this.usernameElement = document.createElement("span");
    this.userEmailElement = document.createElement("a");
    this.descriptionElement = document.createElement("p");
  }

  showCard() {
    let postContent = document.createElement("div");

    let descContainer = document.createElement("div");

    this.post.classList.add("post");
    postContent.classList.add("post__content");
    this.titleElement.classList.add("title");
    this.usernameElement.classList.add("username");
    this.userEmailElement.classList.add("email");
    descContainer.classList.add("description__container");
    this.descriptionElement.classList.add("description");

    this.remover.classList.add("remover");
    this.remover.innerText = "delete post";

    this.editor.classList.add("editor");
    this.editor.innerText = "edit";
    this.editorOk.classList.add("ok");
    this.editorOk.innerText = "ok";

    this.titleElement.innerText = this.title;
    this.usernameElement.innerText = this.name;
    this.userEmailElement.innerText = this.email;
    this.userEmailElement.href = `mailto:${this.email}`;
    this.descriptionElement.innerText = this.body;

    if (this.titleElement.innerText === this.title) {
      loader.remove();
    }

    this.titleElement.append(this.remover);
    descContainer.append(this.descriptionElement, this.editor, this.editorOk);
    postContent.append(
      this.titleElement,
      this.usernameElement,
      this.userEmailElement,
      descContainer
    );
    this.post.append(postContent, this.remover);
    posts.append(this.post);
    this.removeCard();
    this.editCard();
  }

  removeCard() {
    this.remover.addEventListener("click", () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
        method: "DELETE",
      })
        .then(() => {
          this.post.remove();
        })
        .catch((error) => {
          console.error(error);
        });
    });
  }

  editCard() {
    this.editor.addEventListener("click", () => {
      this.editorOk.style.display = "block";
      this.editor.style.display = "none";
      const titleInput = document.createElement("input");
      titleInput.classList.add("title");
      const usernameInput = document.createElement("input");
      usernameInput.classList.add("username");
      const emailInput = document.createElement("input");
      emailInput.classList.add("email");
      const descriptionInput = document.createElement("textarea");
      descriptionInput.classList.add("description");

      titleInput.value = this.title;
      usernameInput.value = this.name;
      emailInput.value = this.email;
      descriptionInput.value = this.body;

      this.titleElement.replaceWith(titleInput);
      this.usernameElement.replaceWith(usernameInput);
      this.userEmailElement.replaceWith(emailInput);
      this.descriptionElement.replaceWith(descriptionInput);

      this.editorOk.addEventListener("click", () => {
        this.editorOk.style.display = "none";
        this.editor.style.display = "block";

        const newTitle = titleInput.value;
        const newUsername = usernameInput.value;
        const newEmail = emailInput.value;
        const newDescription = descriptionInput.value;

        this.titleElement.innerText = newTitle;
        this.usernameElement.innerText = newUsername;
        this.userEmailElement.innerText = newEmail;
        this.descriptionElement.innerText = newDescription;

        titleInput.replaceWith(this.titleElement);
        usernameInput.replaceWith(this.usernameElement);
        emailInput.replaceWith(this.userEmailElement);
        descriptionInput.replaceWith(this.descriptionElement);

        this.title = this.titleElement.innerText;
        this.name = this.usernameElement.innerText;
        this.email = this.userEmailElement.innerText;
        this.body = this.descriptionElement.innerText;

        fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
          method: "PUT",
          body: JSON.stringify({
            title: newTitle,
            body: newDescription,
          }),
        });
      });
    });
  }

  creator() {
    this.showCard();
    posts.prepend(this.post);
  }
}
