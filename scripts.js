"use strict";
import { Card } from "./card.js";
const root = document.getElementById("root");

fetch("https://ajax.test-danit.com/api/json/users", {
  method: "GET",
})
  .then((res) => res.json())
  .then((data) => {
    const users = data.map(({ id, name, email }) => {
      return { id, name, email };
    });
    fetch("https://ajax.test-danit.com/api/json/posts", {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        data.forEach((post) => {
          createPost(users, post);
        });
      });
  });

function createPost(users, post) {
  users.forEach((user) => {
    if (user.id === post.userId) {
      new Card(
        post.title,
        user.name,
        user.email,
        post.body,
        post.id
      ).showCard();
    }
  });
}

const addBtn = document.querySelector(".add");
const modale = document.querySelector(".modale");
const close = document.querySelector(".close");

function buttonListener(button) {
  button.addEventListener("click", () => {
    modale.classList.toggle("modale-active");
  });
}

buttonListener(addBtn);
buttonListener(close);

const creator = document.querySelector(".create");

const inputTitle = document.querySelector(".input_headline");
const inputDescription = document.querySelector(".input_description");

creator.addEventListener("click", () => {
  const createTitle = inputTitle.value;
  const createBody = inputDescription.value;
  new Card(
    createTitle,
    "Leanne Graham",
    "Sincere@april.biz",
    createBody,
    1
  ).creator();
  fetch(`https://ajax.test-danit.com/api/json/posts/`, {
    method: "POST",
    body: JSON.stringify({
      title: createTitle,
      body: createBody,
    }),
  });
});
